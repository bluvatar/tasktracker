﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskTracker.Common.Resources
{
    public static class ExceptionMessageTexts
    {
        public static string MoreThanOneSessionFoundText => "More than one session found.";
    }
}
