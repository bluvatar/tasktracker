﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskTracker.Common.Interfaces
{
    public interface IInitializable
    {
        void Initialize();
    }
}
