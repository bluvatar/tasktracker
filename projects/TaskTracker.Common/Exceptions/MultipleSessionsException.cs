﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskTracker.Common.Exceptions
{
    [Serializable]
    public class MultipleSessionsException : Exception
    {
        public MultipleSessionsException() { }
        public MultipleSessionsException(string message) : base(message) { }
        public MultipleSessionsException(string message, Exception innerException) : base(message, innerException) { }
        protected MultipleSessionsException(System.Runtime.Serialization.SerializationInfo serializationInfo, System.Runtime.Serialization.StreamingContext streamingContext) : base (serializationInfo, streamingContext) { }
    }
}
