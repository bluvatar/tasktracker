﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskTracker.Common.DB;


namespace TaskTracker.Common.Providers
{
    public static class CurrentUserProvider
    {
        private const int _RefreshRateSec = 15;
        private static DateTime _LastRefreshTime = DateTime.MinValue;
        public static string SystemLogin => Environment.UserDomainName + @"\" + Environment.UserName;

        private static User _CurrentUser;
        public static User CurrentUser
        {
            get
            {
                if (_CurrentUser == null || DateTime.Now.AddSeconds((-1) * _RefreshRateSec) > _LastRefreshTime)
                {
                    using (var ctx = new TaskTrackerDBContext())
                    {
                        _CurrentUser = ctx.Users.FirstOrDefault(u => u.SystemLogin == SystemLogin);
                    }
                }
                return _CurrentUser;
            }
        }

        private static bool _CanRunApp;
        public static bool CanRunApp
        {
            get
            {
                if (DateTime.Now.AddSeconds((-1) * _RefreshRateSec) > _LastRefreshTime)
                {
                    using (TaskTrackerDBContext ctx = new TaskTrackerDBContext())
                    {
                        _CurrentUser = ctx.Users.FirstOrDefault(u => u.SystemLogin == SystemLogin);
                        _CanRunApp = _CurrentUser != null && _CurrentUser.UserAccesses.FirstOrDefault(acc => acc.Access.Name == "RUN") != null;
                    }
                }
                return _CanRunApp;
            }
        }

        private static bool _IsAdmin;
        public static bool IsAdmin
        {
            get
            {
                if (DateTime.Now.AddSeconds((-1) * _RefreshRateSec) > _LastRefreshTime)
                {
                    using (TaskTrackerDBContext ctx = new TaskTrackerDBContext())
                    {
                        _CurrentUser = ctx.Users.FirstOrDefault(u => u.SystemLogin == SystemLogin);
                        _IsAdmin = _CurrentUser != null && _CurrentUser.UserAccesses.FirstOrDefault(acc => acc.Access.Name == "IS_ADMIN") != null;
                    }
                }
                return _IsAdmin;
            }
        }
    }
}
