﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.Common.DB;
using TaskTracker.Common.Models;

namespace TaskTracker.Common.Handlers
{
    public static class UserEventHandler
    {
        public static IEnumerable<UserEventModel> GetTodayEvents(int userId)
        {
            using (TaskTrackerDBContext ctx = new TaskTrackerDBContext())
            {
                if (ctx.UserEvents.Where(ev => ev.UserId == userId && !ev.Session.SessionFinished).Select(sessionId => sessionId.Session.Id).Count() > 1)
                {
                    throw new Exceptions.MultipleSessionsException(Resources.ExceptionMessageTexts.MoreThanOneSessionFoundText);
                }
                foreach (var uEvent in ctx.UserEvents.Where(ev => ev.UserId == userId && !ev.Session.SessionFinished))
                {
                    yield return GetModelFromEntity(uEvent);
                }
            }
        }

        public static async Task<UserEventModel> AddNewEvent(int userId, int configEventId, DateTime eventStartTime)
        {
            using (TaskTrackerDBContext ctx = new TaskTrackerDBContext())
            {
                var newEvent = ctx.UserEvents.Add(new UserEvent()
                {
                    UserId = userId,
                    ConfigEventId = configEventId,
                    StartTime = eventStartTime,
                    LastUpdateTime = eventStartTime,
                });
                await ctx.SaveChangesAsync().ConfigureAwait(true);

                return GetModelFromEntity(newEvent);
            }
        }

        private static UserEventModel GetModelFromEntity(UserEvent uEvent)
        {
            return new UserEventModel()
            {
                UserEventId = uEvent.Id,
                UserId = uEvent.UserId,
                UserFullName = uEvent.User.FullName,
                UserSystemLogin = uEvent.User.SystemLogin,
                ConfigEventId = uEvent.ConfigEventId,
                EventName = uEvent.Event.Name,
                StartTime = uEvent.StartTime,
                EndTime = uEvent.EndTime,
                LastUpdateTime = uEvent.LastUpdateTime
            };
        }
    }
}
