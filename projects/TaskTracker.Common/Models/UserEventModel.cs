﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskTracker.Common.Models
{
    public class UserEventModel
    {
        public int? UserEventId { get; set; }

        public int UserId { get; set; }
        public string UserFullName { get; set; }
        public string UserSystemLogin { get; set; }

        public int ConfigEventId { get; set; }
        public string EventName { get; set; }

        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public DateTime LastUpdateTime { get; set; }

        public TimeSpan Duration => EndTime != null ? EndTime.Value - StartTime : DateTime.Now - StartTime;


        public void CloseEvent()
        {
        }

        public void UpdateEvent()
        {

        }
    }
}
