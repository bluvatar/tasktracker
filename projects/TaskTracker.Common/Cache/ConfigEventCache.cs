﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.Common.DB;

namespace TaskTracker.Common.Cache
{
    public static class ConfigEventCache
    {
        private const int _RefreshRateSec = 15;
        private static DateTime _LastRefreshTime = DateTime.MinValue;
        private static Dictionary<int, ConfigEvent> _ConfigEvents;
        public static Dictionary<int, ConfigEvent> ConfigEventsDict
        {
            get
            {
                if (_ConfigEvents == null) _ConfigEvents = new Dictionary<int, ConfigEvent>();
                if (DateTime.Now.AddSeconds((-1) * _RefreshRateSec) > _LastRefreshTime)
                {
                    using (var ctx = new TaskTrackerDBContext())
                    {
                        var configEventsEntities = ctx.ConfigEvents.Where(cEv => true); // get all
                        _ConfigEvents.Clear();
                        foreach(var evEnt in configEventsEntities)
                        {
                            _ConfigEvents.Add(evEnt.Id, evEnt);
                        }
                    }
                }
                return _ConfigEvents;
            }
        }
        public static IEnumerable<ConfigEvent> ConfigEvents
        {
            get
            {
                foreach(var dictItem in ConfigEventsDict)
                {
                    yield return dictItem.Value;
                }
            }
        }
    }
}
