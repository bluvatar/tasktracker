﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TaskTracker.Common.Interfaces;

namespace TaskTracker.GUI.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            InitializeInitializableResources();
        }
        private void InitializeInitializableResources()
        {
            foreach (var res in Resources)
            {
                if (!(res is DictionaryEntry entry)) continue;
                if (!(entry.Value is IInitializable initializable)) continue;
                initializable.Initialize();
            }
        }
    }
}