﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

using TaskTracker.Common.Providers;

namespace TaskTracker.GUI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static void Shutdown(int? exitCode = null)
        {
            if (exitCode != null) App.Current.Shutdown((int)exitCode);
            else App.Current.Shutdown();
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            if (!CurrentUserProvider.CanRunApp)
            {
                MessageBox.Show($"You are not allowed to run this app with LOGIN '{CurrentUserProvider.SystemLogin}'");
                App.Shutdown();
            }
        }
    }
}
