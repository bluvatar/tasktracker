﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "Conflicting BullShit", Scope = "member", Target = "~P:TaskTracker.GUI.ViewModels.MainStatusBarViewModel.CurrentTime")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "Conflicting BullShit", Scope = "type", Target = "~T:TaskTracker.GUI.ViewModels.MainStatusBarViewModel")]
