﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Timers;
using TaskTracker.Common.Providers;
using TaskTracker.Common.Interfaces;

namespace TaskTracker.GUI.ViewModels
{
    public class MainStatusBarViewModel : INotifyPropertyChanged, IInitializable
    {
        public string UserFullName => CurrentUserProvider.CurrentUser.FullName;
        public string UserSystemLogin => CurrentUserProvider.CurrentUser.SystemLogin;
        public string CurrentTime => DateTime.Now.ToString("HH:mm:ss");

        public MainStatusBarViewModel()
        {
        }

        public void Initialize()
        {
            SetupTimer();
        }

        private Timer _Timer;
        private void SetupTimer()
        {
            if (_Timer == null)
            {
                _Timer = new Timer()
                {
                    Interval = 500,
                    Enabled = true,
                };
            }
            _Timer.Start();
            _Timer.Elapsed += (s, e) =>
             {
                 NotifyPropertyChanged(nameof(CurrentTime));
             };
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
